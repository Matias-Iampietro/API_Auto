package Workflow.Controllers;

import Workflow.Model.Auto;
import Workflow.Model.ColeccionAutos;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Cecilia on 24/5/2017.
 */

@RestController
public class AutosController {

    private ColeccionAutos map = new ColeccionAutos();
    private ArrayList<Auto> aux = new ArrayList<Auto>();

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<Auto> getAllCars() {
        return map.getColeccion();
    }

    @RequestMapping(value = "/Autos/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Auto getOneCar(@PathVariable("id") int id) {
        Auto a= map.getById(id);
        return a;
    }

    @RequestMapping(value = "/byMarca", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<Auto> getAFewCars(@RequestParam("marca") String marc) {
        for (Auto a : map.getColeccion()){
            if (a.getMarca().getDescripcion().equals(marc)){
                aux.add(a);
            }
        }
        return aux;
    }

    @RequestMapping(value = "/Auto", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void addCar(@RequestBody Auto a) {
        map.saveCar(a);
    }


}
