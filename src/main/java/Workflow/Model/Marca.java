package Workflow.Model;

/**
 * Created by Cecilia on 24/5/2017.
 */
public class Marca {
    private String descripcion;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Marca(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public Marca(){ this.descripcion = ""; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Marca)) return false;

        Marca marca = (Marca) o;

        return getDescripcion() != null ? getDescripcion().equals(marca.getDescripcion()) : marca.getDescripcion() == null;
    }

    @Override
    public int hashCode() {
        return getDescripcion() != null ? getDescripcion().hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Marca{" +
                "descripcion='" + descripcion + '\'' +
                '}';
    }
}
