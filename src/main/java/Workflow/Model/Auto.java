package Workflow.Model;

import java.util.HashMap;

/**
 * Created by Cecilia on 24/5/2017.
 */
public class Auto {

    private Marca marca;
    private String modelo;
    private Integer kilometros;
    private String patente;
    private Integer anio;
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Integer getKilometros() {
        return kilometros;
    }

    public void setKilometros(Integer kilometros) {
        this.kilometros = kilometros;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public Auto(Marca marca, String modelo, Integer kilometros, String patente, Integer anio, Integer ID)
    {
        this.marca = marca;
        this.modelo = modelo;
        this.kilometros = kilometros;
        this.patente = patente;
        this.anio = anio;
        this.id = ID;
    }

    public Auto(){
        this.marca = new Marca();
        this.modelo = "";
        this.kilometros = 0;
        this.patente = "";
        this.anio = 0;
        this.id = 0;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Auto)) return false;

        Auto auto = (Auto) o;

        if (getMarca() != null ? !getMarca().equals(auto.getMarca()) : auto.getMarca() != null) return false;
        if (getModelo() != null ? !getModelo().equals(auto.getModelo()) : auto.getModelo() != null) return false;
        if (getKilometros() != null ? !getKilometros().equals(auto.getKilometros()) : auto.getKilometros() != null)
            return false;
        if (getPatente() != null ? !getPatente().equals(auto.getPatente()) : auto.getPatente() != null) return false;
        if (getAnio() != null ? !getAnio().equals(auto.getAnio()) : auto.getAnio() != null) return false;
        return getId() != null ? getId().equals(auto.getId()) : auto.getId() == null;
    }

    @Override
    public int hashCode() {
        int result = getMarca() != null ? getMarca().hashCode() : 0;
        result = 31 * result + (getModelo() != null ? getModelo().hashCode() : 0);
        result = 31 * result + (getKilometros() != null ? getKilometros().hashCode() : 0);
        result = 31 * result + (getPatente() != null ? getPatente().hashCode() : 0);
        result = 31 * result + (getAnio() != null ? getAnio().hashCode() : 0);
        result = 31 * result + (getId() != null ? getId().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Auto{" +
                "marca=" + marca +
                ", modelo='" + modelo + '\'' +
                ", kilometros=" + kilometros +
                ", patente='" + patente + '\'' +
                ", anio=" + anio +
                ", id=" + id +
                '}';
    }
}
