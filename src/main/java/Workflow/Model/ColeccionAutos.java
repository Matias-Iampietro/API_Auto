package Workflow.Model;

import java.util.*;

/**
 * Created by Cecilia on 24/5/2017.
 */
public class ColeccionAutos {
    private ArrayList<Auto> coleccion = new ArrayList<Auto>();

    public ColeccionAutos() {
        Marca m = new Marca("Ford");
        Marca m1 = new Marca("Chevrolet");
        Marca m2 = new Marca("Volkswagen");

        Auto unAuto = new Auto(m, "Fiesta", 1234, "ASD345", 1993, 1);
        Auto otroAuto = new Auto(m1, "S10", 34547, "TKJ835", 2007, 2);
        Auto anotherCar = new Auto(m2, "Golf", 6669, "JRC825", 1984, 3);

        coleccion.add(unAuto);
        coleccion.add(otroAuto);
        coleccion.add(anotherCar);
    }

    public ArrayList<Auto> getColeccion() {
        return coleccion;
    }

    public void setColeccion(ArrayList<Auto> coleccion) {
        this.coleccion = coleccion;
    }

    public void mostrarColeccion(){
        for (Auto a : coleccion){
            System.out.println(a);
        }
    }

    public void saveCar(Auto a){
        coleccion.add(a);
    }

    public Auto getById(int id){
        Auto aux = new Auto();
        for (Auto a : coleccion){
            if (a.getId() == id){
                aux = a;
            }
        }
        return aux;
    }

}
